package com.pixelheart.learnandroidarchictectur;

import com.pixelheart.learnandroidarchictectur.Auth.AuthDependencyContainer;
import com.pixelheart.learnandroidarchictectur.Auth.AuthFragment;

public class MainDependencyContainer {
    public MainViewModel mainViewModel;

    MainDependencyContainer() {
        this.mainViewModel = makeMainViewModel().create();
    }
    MainViewModelFactory makeMainViewModel() {
        return new MainViewModelFactory();
    }

    public AuthFragment makeAuthFragment() {
        return new AuthDependencyContainer(this).makeAuthFragment();
    }
}

