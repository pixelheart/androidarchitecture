package com.pixelheart.learnandroidarchictectur.Auth;

import com.pixelheart.learnandroidarchictectur.Createable;

public class RegisterFragmentFactory implements Createable<RegisterFragment> {
    @Override
    public RegisterFragment create() {
        return RegisterFragment.newInstance("1", "2");
    }
}
