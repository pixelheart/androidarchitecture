package com.pixelheart.learnandroidarchictectur.Auth;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;

import com.pixelheart.learnandroidarchictectur.Createable;
import com.pixelheart.learnandroidarchictectur.MainDependencyContainer;
import com.pixelheart.learnandroidarchictectur.MainViewModel;
import com.pixelheart.learnandroidarchictectur.R;

public class AuthFragment extends Fragment {

    public AuthViewModel authViewModel;

    public LoginFragmentFactory loginFragmentFactory;

    public RegisterFragmentFactory registerFragmentFactory;

    private LoginFragment loginFragment;

    View view;
    Button loginButton;
    Button registerButton;

    public AuthFragment(
            AuthViewModel authViewModel,
            LoginFragmentFactory loginFragmentFactory,
            RegisterFragmentFactory registerFragmentFactory
    ) {
        // Required empty public constructor
        this.authViewModel = authViewModel;
        this.loginFragmentFactory = loginFragmentFactory;
        this.registerFragmentFactory = registerFragmentFactory;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_auth, container, false);
        loginButton = view.findViewById(R.id.login_btn);
        registerButton = view.findViewById(R.id.register_btn);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginFragmentFactory.create();
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerFragmentFactory.create();
            }
        });

        return view;
    }
}