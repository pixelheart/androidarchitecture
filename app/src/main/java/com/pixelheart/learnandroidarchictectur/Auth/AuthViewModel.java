package com.pixelheart.learnandroidarchictectur.Auth;

import android.util.Log;

import androidx.lifecycle.ViewModel;

import com.pixelheart.learnandroidarchictectur.Kit.Responder.SignedInResponder;

public class AuthViewModel extends ViewModel {

    public SignedInResponder signedInResponder;

    AuthViewModel(SignedInResponder signedInResponder){
        this.signedInResponder = signedInResponder;
    }

    public void home(){
        signedInResponder.gotoSignedIn();
    }

}


