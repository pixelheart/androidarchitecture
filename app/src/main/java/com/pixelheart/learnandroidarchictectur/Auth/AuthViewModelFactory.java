package com.pixelheart.learnandroidarchictectur.Auth;

import android.util.Log;

import com.pixelheart.learnandroidarchictectur.Kit.Factory;
import com.pixelheart.learnandroidarchictectur.Kit.Responder.SignedInResponder;

public class AuthViewModelFactory implements Factory {

    private final SignedInResponder signedInResponder;

    AuthViewModelFactory( SignedInResponder signedInResponder){
        this.signedInResponder = signedInResponder;
    }

    @Override
    public AuthViewModel create() {
        return new AuthViewModel(signedInResponder);
    }
}
