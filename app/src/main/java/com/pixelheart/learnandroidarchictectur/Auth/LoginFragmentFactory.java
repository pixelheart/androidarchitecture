package com.pixelheart.learnandroidarchictectur.Auth;

import com.pixelheart.learnandroidarchictectur.Createable;

public class LoginFragmentFactory implements Createable<LoginFragment> {

    private AuthViewModel sharedViewModel;
    public LoginViewModel loginViewModel;

    LoginFragmentFactory(AuthViewModel sharedViewModel,
                         LoginViewModel loginViewModel) {
        this.sharedViewModel = sharedViewModel;
        this.loginViewModel = loginViewModel;
    }
    @Override
    public LoginFragment create() {
        return LoginFragment.newInstance(loginViewModel, "1", "2");
    }
}
