package com.pixelheart.learnandroidarchictectur.Auth;

import android.util.Log;

import com.pixelheart.learnandroidarchictectur.Createable;
import com.pixelheart.learnandroidarchictectur.MainDependencyContainer;
import com.pixelheart.learnandroidarchictectur.MainViewModel;

public class AuthDependencyContainer {
    private final MainViewModel shareMainViewModel;
    public AuthViewModel authViewModel;

    public AuthDependencyContainer(MainDependencyContainer container) {
        this.shareMainViewModel = container.mainViewModel;
        this.authViewModel = makeAuthViewModelFactory().create();
    }

    private AuthViewModelFactory makeAuthViewModelFactory() {
        return new AuthViewModelFactory(shareMainViewModel);
    }

    public AuthFragment makeAuthFragment() {
        return new AuthFragment(
                authViewModel,
                makeLoginFragment(),
                makeRegisterFragment()
        );
    }

    private LoginFragmentFactory makeLoginFragment() {
        LoginViewModel loginViewModel = new LoginViewModel();
        return new LoginFragmentFactory(authViewModel, loginViewModel);
    }

    private RegisterFragmentFactory makeRegisterFragment() {
        return new RegisterFragmentFactory();
    }

}

