package com.pixelheart.learnandroidarchictectur.Kit;

public interface Factory<T> {
    T create();
}
