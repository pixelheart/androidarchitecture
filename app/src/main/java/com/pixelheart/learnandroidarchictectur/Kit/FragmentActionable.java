package com.pixelheart.learnandroidarchictectur.Kit;

import androidx.fragment.app.Fragment;

public interface FragmentActionable {
    void addFragment(Fragment fragment, String tag);
    void removeFragment(String tag);
}
