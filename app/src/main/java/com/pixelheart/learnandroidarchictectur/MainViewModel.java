package com.pixelheart.learnandroidarchictectur;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.pixelheart.learnandroidarchictectur.Kit.Responder.AuthResponder;
import com.pixelheart.learnandroidarchictectur.Kit.Responder.SignedInResponder;

public class MainViewModel extends ViewModel implements AuthResponder, SignedInResponder {
    private MutableLiveData<MainView> viewState;

    public MutableLiveData<MainView> getViewState(){
        if (viewState == null) {
                viewState = new MutableLiveData<MainView>(MainView.SIGNIN_VIEW);
        }
        return viewState;
    }

    public void printString() {
        Log.d("mainViewModel", "kenek rek");
    }

    @Override
    public void gotoSignIn() {
        viewState.setValue(MainView.SIGNIN_VIEW);
    }

    @Override
    public void gotoSignedIn() {
        viewState.setValue(MainView.SIGNEDIN_VIEW);
    }
}

