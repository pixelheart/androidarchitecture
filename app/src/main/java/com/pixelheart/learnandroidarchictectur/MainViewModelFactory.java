package com.pixelheart.learnandroidarchictectur;

import com.pixelheart.learnandroidarchictectur.Kit.Factory;

public class MainViewModelFactory implements Factory {
    @Override
    public MainViewModel create() {
        return new MainViewModel();
    }
}
