package com.pixelheart.learnandroidarchictectur;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.fragment.app.Fragment;

import com.pixelheart.learnandroidarchictectur.Auth.AuthFragment;
import com.pixelheart.learnandroidarchictectur.Kit.FragmentActionable;

public class MainActivity extends AppCompatActivity implements FragmentActionable {

    private MainViewModel mainViewModel;
    private AuthFragment authFragment;

    //Views
    Button nextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nextButton = findViewById(R.id.next_button);

        //Get dependency
        MainDependencyContainer appContainer = ((MainApplication) getApplication()).mainDependencyContainer;
        this.mainViewModel = appContainer.mainViewModel;
        this.authFragment = appContainer.makeAuthFragment();

        observeViewModel();

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainViewModel.gotoSignIn();
            }
        });
    }

    void observeViewModel() {
        final Observer<MainView> viewStateObserver = new Observer<MainView>() {
            @Override
            public void onChanged(MainView mainView) {
                Log.d("main", "view state : " + mainView);
                switch (mainView) {
                    case MAIN_VIEW:
                        break;
                    case SIGNEDIN_VIEW:
                        Log.d("main", "observeViewModel: move to HOME VIEW");
                        removeFragment("FIRST");
                        break;
                    case SIGNIN_VIEW:
                        addFragment(authFragment, "FIRST");
                        break;
                }
            }
        };

        mainViewModel.getViewState().observe(this, viewStateObserver);
    }

    @Override
    public void addFragment(Fragment fragment, String tag) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        if (manager.findFragmentByTag(tag) != null) {
            Log.d("fragment", "fragment already added");
            return;
        }
        transaction.add(R.id.frame_layout, fragment, tag);
        transaction.commit();
    }

    @Override
    public void removeFragment(String tag) {
        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(tag);
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.remove(fragment);
        transaction.commit();
    }

}