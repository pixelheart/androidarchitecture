package com.pixelheart.learnandroidarchictectur;

public interface Createable<T> {
    T create();
}
